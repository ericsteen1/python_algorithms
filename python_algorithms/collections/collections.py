from collections import defaultdict
# namedtuple()
# UserDict
# UserList
# UserString
# OrderedDict
# counter
# ChainMap
# deque

# sequence types:
# tuple, list, string, range

# unpacking sequences
p = 'ray', 0x30, 'python'

# no:
name = p[0]
hex = p[1]
# yes:
name, hex = p
